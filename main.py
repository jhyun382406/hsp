import os
import datetime
import argparse
from setproctitle import setproctitle
from pytz import timezone

from utils.log_module import LogConfig, get_log_view
from utils.code_utils import convert


def data_raw(cfg, kst_start_time, kst_end_time, log, err_log):
    from src.extract_data import ExtractRawRadar
    
    log.info("--------------------------------------------------")
    err_log.error("--------------------------------------------------")
    
    # Parsing Radar & Save zarr
    radar = ExtractRawRadar(cfg, log, err_log)
    radar(kst_start_time, kst_end_time)


def data_crop(cfg, log, err_log):
    from src.extract_data import CropRadar
    
    log.info("--------------------------------------------------")
    err_log.error("--------------------------------------------------")
    
    # Crop area & Make dat
    crdr = CropRadar(cfg, log, err_log)
    crdr()


def data_maxp(cfg, log, err_log):
    from src.extract_data import MaxpoolRadar
    
    log.info("--------------------------------------------------")
    err_log.error("--------------------------------------------------")
    
    # Maxpool area & Make dat
    mrdr = MaxpoolRadar(cfg, log, err_log)
    mrdr()


def data_embd(cfg, log, err_log):
    from src.extract_data import EmbedRadar
    
    log.info("--------------------------------------------------")
    err_log.error("--------------------------------------------------")
    
    # Embed area & Make zarr
    erdr = EmbedRadar(cfg, log, err_log)
    erdr()


def setting_log(cfg,
                upper_dir,
                log_level="INFO",
                err_log_level="WARNING"):
    """
    Description:
        로그 객체 생성.
    Args:
        cfg: Namedtuple, YAML 파일에서 읽어온 설정
        upper_dir: str, 해당 파일의 상위 디렉토리 경로
        log_level: str, 로그 레벨 (default="INFO")
            - DEBUG -> INFO -> WARNING -> ERROR -> CRITICAL
        err_log_level: str, 에러 로그 레벨 (default="WARNING")
            - DEBUG -> INFO -> WARNING -> ERROR -> CRITICAL
    Returns:
        log: logging.logger, 로그 객체
        err_log: logging.logger, 에러 로그 객체
    """
    file_prefix = cfg.LOG_PREFIX
    lc = LogConfig(upper_dir, file_prefix)
    log = get_log_view(lc, log_level=log_level)   # default: INFO
    err_log = get_log_view(lc, log_level=err_log_level, error_log=True)
    return log, err_log


def main(args):
    """
    Description:
        메인 모듈 실행함수.
        data 모드일 땐, start_time과 end_time 설정이 필수.
        사용 예: python main.py -c ./configs/hsp_cfg.yaml -m train
                 python main.py -c ./configs/hsp_cfg.yaml -m data-raw -st 202203150700 -et 202203150850
    Args:
        args: 파이썬 실행할 때, 입력한 값
              "-c", "--config": 설정 yaml 파일 경로
              "-m", "--mode": 모드 변경
              "-st", "--start_time", 시작 연월일시분 (KST), e.g. 202203150700
              "-et", "--end_time", 마지막 연월일시분 (KST), e.g. 202203150850
    """
    upper_dir = os.path.dirname(os.path.abspath(__file__))
    cfg_file = args.config
    cfg = convert(cfg_file)

    if args.mode == "data-raw":
        setproctitle("extract_raw")
        log, err_log = setting_log(cfg.RAWDATA, upper_dir)
        kst_start_time = args.start_time
        kst_end_time = args.end_time if args.end_time is not None else kst_start_time
        data_raw(cfg, kst_start_time, kst_end_time, log, err_log)
    
    elif args.mode == "data-crop":
        setproctitle("crop_raw")
        log, err_log = setting_log(cfg.RAWDATA, upper_dir)
        data_crop(cfg, log, err_log)
    
    elif args.mode == "data-maxp":
        setproctitle("maxpool_raw")
        log, err_log = setting_log(cfg.RAWDATA, upper_dir)
        data_maxp(cfg, log, err_log)
    
    elif args.mode == "data-embed":
        setproctitle("embed_raw")
        log, err_log = setting_log(cfg.RAWDATA, upper_dir)
        data_embd(cfg, log, err_log)


if __name__ == "__main__":
    from src.argparser import make_args
    
    parser = argparse.ArgumentParser()
    args = make_args(parser)
    
    main(args)
