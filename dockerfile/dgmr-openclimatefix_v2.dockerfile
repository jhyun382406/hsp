FROM pytorch/pytorch:2.0.1-cuda11.7-cudnn8-runtime

# opencv 또는 matplotlib 관련 라이브러리
RUN apt update && apt install -y vim sudo libgl1-mesa-glx libglib2.0-0

# USER 추가 및 sudo 권한 설정 (CHPASSWD는 계정:비밀번호 구조)
ARG USER="wizai" \
  CHPASSWD="wizai:wizai"
RUN adduser --disabled-password --gecos "" $USER \
  && echo $CHPASSWD | chpasswd \
  && adduser $USER sudo \
  && echo '$USER ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers \
# root의 .bashrc 복사 후 USER 변경
  && cat /root/.bashrc >> /home/$USER/.bashrc \
  && chown $USER:$USER /home/$USER/.bashrc

# pip PATH 지정 안되어 추가
RUN python -m pip install --upgrade pip \
  && pip install setproctitle \
  numpy==1.26.3 \
  pandas==2.1.4 \
  scipy==1.11.4 \
  scikit-learn==1.3.2 \
  dask==2024.1.1 \
  opencv-python==4.8.0.74 \
  matplotlib==3.7.4 \
  xarray==2024.1.1 \
  zarr==2.16.1 \
  antialiased-cnns==0.3 \
  pytorch-msssim==1.0.0 \
  pytorch-lightning==2.1.3 \
  einops==0.7.0 \
  huggingface-hub==0.19.4 \
  datasets==2.14.7 \
  torchinfo==1.8.0 \
  ipykernel==6.26.0 \
  ray==2.8.1

USER $USER
ENV PATH=/home/$USER/.local/bin:$PATH

# key value 식으로 이미지에 코멘트 작성
LABEL title="DGMR" \
  version="openclimatefix_240201" \
  baseimage="pytorch/pytorch:2.0.1-cuda11.7-cudnn8-runtime" \
  description="Test DGMR using HSP environment"
