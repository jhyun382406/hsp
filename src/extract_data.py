"""
HSP 파일 파싱.

Raw 데이터는 1차적으로 zarr로 만들고(int16, 원래 수의 x100),
이후 추가 작업 결과는 dat으로 만들기(float32)

클래스 설명
    - Data: 파일 시간대 관련
    - RadarHSP: HSP 데이터 처리 상위 클래스
    - ExtractRawRadar: bin.gz -> zarr 또는 dat 저장. 레이더 영역만 슬라이싱.
    - CropRadar: zarr-> dat 저장. 일부 영역만 crop.
    - MaxpoolRadar: zarr -> dat 저장. padding 후 maxpooling 적용.
    - EmbedRadar: zarr -> dat 저장. padding 후 patch embedding 아이디어 적용하여 원본 데이터 보존.
"""
import os
from datetime import datetime, timedelta
import time
import gzip
import json
from collections import Counter
import numpy as np
import zarr
import dask.array as da
import ray

from utils.log_module import print_elapsed_time
from utils.code_utils import save_json, load_json
from utils.ray_setting import ray_start, ray_stop


class Data(object):
    """공통 부분."""
    def __init__(self):
        """초기값."""
        super(Data, self).__init__()
        self.CVT_TIME_FORMAT = "%Y%m%d%H%M"
        self.UTC2KST = 9
        self.KST2UTC = self.UTC2KST * (-1)
    
    def _kst_to_utc(self, date):
        """
        Description:
            KST 기준 시간을 UTC로 변경.
        Args:
            date: str, KST 연월일시분(12자리)
        Returns:
            date: str, UTC 연월일시분(12자리)
        """
        date = datetime.strptime(str(date), self.CVT_TIME_FORMAT)
        date += timedelta(hours=self.KST2UTC)
        date = datetime.strftime(date, self.CVT_TIME_FORMAT)
        return date

    def _utc_to_kst(self, date):
        """
        Description:
            UTC 기준 시간을 KST로 변경.
        Args:
            date: str, UTC 연월일시분(12자리)
        Returns:
            date: str, KST 연월일시분(12자리)
        """
        date = datetime.strptime(str(date), self.CVT_TIME_FORMAT)
        date += timedelta(hours=self.UTC2KST)
        date = datetime.strftime(date, self.CVT_TIME_FORMAT)
        return date

    def _compute_date(self, date, standard, time_step):
        """
        Description:
            분단위 기준으로 이후 시간 계산.
        Args:
            date: str, 연월일시분(12자리)
            standard: str, day, hour, min, sec 만 사용 가능
            time_step: int, standard에서 증감할 시간 간격(양수면 이후, 음수면 이전)
        Returns:
            date: str, 연월일시분(12자리)
        """
        date = datetime.strptime(str(date), self.CVT_TIME_FORMAT)
        if standard == "day":
            date += timedelta(days=time_step)
        elif standard == "hour":
            date += timedelta(hours=time_step)
        elif standard == "min":
            date += timedelta(minutes=time_step)
        elif standard == "sec":
            date += timedelta(seconds=time_step)
        else:
            print("Wrong time step standard: {0}".format(standard))
            print("Just using: day, hour, min, sec")
        date = datetime.strftime(date, self.CVT_TIME_FORMAT)
        return date

    def _make_time_list(self, start_time, end_time, standard, time_step):
        """
        Description:
            시간대 리스트 생성.
        Args:
            start_time: str, 연월일시분(12자리)
            end_time: str, 연월일시분(12자리)
            standard: str, day, hour, min, sec 만 사용 가능
            time_step: int, standard에서 증감할 시간 간격
        Returns:
            time_list: list, 문자열 시간대 리스트
        """
        time_list = []
        while start_time <= end_time:
            time_list.append(start_time)
            start_time = self._compute_date(start_time, standard, time_step)
        return time_list


class RadarHSP(Data):
    def __init__(self, cfg, log, err_log):
        """
        Description:
            레이더 데이터(HSP) 불러오는 init 설정.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(RadarHSP, self).__init__()
        cfg_r = cfg.RAWDATA
        cfg_d = cfg.PARAMS.DATA
        
        # raw mode: Radar file (HSP)
        self.USE_RAY = cfg_r.USE_RAY
        self.NX = cfg_r.NX
        self.NY = cfg_r.NY
        self.HEADER_LEN = int(cfg_r.HEADER_BYTE / 2)
        self.rdr_dir = cfg_r.RADAR_DIR
        self.rdr_file = cfg_r.RADAR_FILE
        self.MAX_LOOP = cfg_r.MAX_LOOP
        self.ARRAY_TYPE_STR = cfg_r.ARRAY_TYPE
        self.ARRAY_TYPE = getattr(np, cfg_r.ARRAY_TYPE)
        self.TIME_STEP = cfg_r.TIME_STEP
        self.FILLNA_TIME_STEP = cfg_r.FILLNA_TIME_STEP * (-1)
        self.TIME_UNIT = cfg_r.TIME_UNIT.lower()
        
        # raw mode: Effective Radar Area (Rectangle)
        self.IPT_H = cfg_d.IPT_H
        self.IPT_W = cfg_d.IPT_W
        self.EDGE_UP = cfg_d.RADAR_EDGE.UP
        self.EDGE_DOWN = cfg_d.RADAR_EDGE.DOWN
        self.EDGE_LEFT = cfg_d.RADAR_EDGE.LEFT
        self.EDGE_RIGHT = cfg_d.RADAR_EDGE.RIGHT
        self.RDR_H = self.EDGE_DOWN - self.EDGE_UP
        self.RDR_W = self.EDGE_RIGHT - self.EDGE_LEFT

        # raw mode: zarr option
        self.use_zarr = cfg_r.ZARR_GROUP.USE
        self.store_kind = cfg_r.ZARR_GROUP.STORE
        self.grp_rdr = cfg_r.ZARR_GROUP.RADAR
        self.grp_kst = cfg_r.ZARR_GROUP.KST
        self.saved_dir = cfg_r.SAVED_DIR.BASE
        self.saved_file = cfg_r.SAVED_FILE.RAW
        
        # raw -> crop, maxpool, embedding
        self.usage_extract = sorted(cfg_r.USAGE_EXTRACT)
        self.saved_usage_file = cfg_r.SAVED_USAGE_FILE
        
        self.log, self.err_log = log, err_log
    
    def __call__(self):
        pass
    
    def _get_save_path(self,
                       kst_start_time,
                       kst_end_time,
                       time_step,
                       time_unit,
                       *args):
        """
        Description:
            저장 경로.
        Args:
            kst_start_time: str, KST 연월일시분(12자리)
            kst_end_time: str, KST 연월일시분(12자리)
            time_step: int, 시간 간격
            time_unit: str, 시간 간격 단위
            *args: str, 저장 경로 os.path.join할 순서대로 배치
        Returns:
            save_path: str, 파일 저장 경로
        """
        save_path = os.path.join(*args)
        save_path = save_path.format(
            time_step, time_unit,
            kst_start_time, kst_end_time
        )
        return save_path
    
    def slice_rdr(self, rdr_ary, UP, DOWN, LEFT, RIGHT):
        """
        Description:
            레이더 데이터 배열에서 일정 크기만 잘라내기.
            (UP~DOWN, LEFT~RIGHT) 영역
        Args:
            rdr_ary: numpy array, 레이더 강수량 배열 (NY, NX)
            UP, DOWN, LEFT, RIGHT: int, 슬라이싱할 영역 픽셀 index
        Returns:
            rdr_ary: numpy array, Slicing한 레이더 강수량 배열 (DOWN - UP, RIGHT - LEFT)
        """
        return rdr_ary[UP:DOWN, LEFT:RIGHT]
    
    def save_zarr(self, save_path, zarr_shape, grp_rdr, grp_kst):
        """
        Description:
            zarr를 저장.
            group 등 기타 옵션은 init에서 self로 가져오기.
            subgroup은 <grp>.create_group("<subgrp_name>") 으로 생성.
            zarr_rdr
                └─ rdr_ary: zarr array, (time_list_len, h, w, 1)
                └─ kst_dt: zarr array, (time_list_len,)
        Args:
            save_path: str, 파일 저장 경로
            zarr_shape: tuple, 저장할 레이더 shape
            grp_rdr: namedtuple, Zarr 레이더 설정 
            grp_kst: namedtuple, Zarr 시간대 설정
        Returns:
            zarr_rdr: zarr group, 저장할 zarr 그룹
            store: zarr ZipStore, 파일 저장 인스턴스
        """
        if self.store_kind == "zip":
            save_path = save_path + "_zarr.zip"
            store = zarr.ZipStore(save_path, mode="w")
        elif self.store_kind == "dir":
            save_path = save_path + "_zarr"
            store = zarr.DirectoryStore(save_path)
        else:
            save_path = save_path + "_zarr"
            store = zarr.DirectoryStore(save_path)
        zarr_rdr = zarr.group(store=store)
        
        # HSP radars
        rdr_chunk = list(zarr_shape)
        rdr_chunk[0] = grp_rdr.CHUNK
        zarr_rdr.zeros(
            grp_rdr.NAME,
            shape=zarr_shape,
            chunks=tuple(rdr_chunk),
            dtype=grp_rdr.DTYPE,
            overwrite=True,
            # compression="none",   # 압축X
        )
        # KST times
        zarr_rdr.zeros(
            grp_kst.NAME,
            shape=(zarr_shape[0],),
            chunks=grp_kst.CHUNK,
            dtype=grp_kst.DTYPE,
            overwrite=True,
        )
        
        return zarr_rdr, store
    
    def load_zarr(self, save_path):
        """
        Description:
            저장한 zarr 불러오기.
        Args:
            save_path: str, 파일 저장 경로
        Returns:
            zarr_rdr: zarr group, 저장할 zarr 그룹
            store: zarr ZipStore, 파일 저장 인스턴스
        """
        if save_path.endswith("zip"):
            store = zarr.ZipStore(save_path, mode="r")
        else:
            store = zarr.DirectoryStore(save_path)
        zarr_rdr = zarr.group(store=store)
        return zarr_rdr, store
    
    def quit_zarr(self, store):
        """
        Description:
            저장한 zarr 파일과 연결 해제.
        Args:
            store: zarr ZipStore, 파일 저장 인스턴스
        Returns:
            None
        """
        store.close()
    
    def assign_zarr(self,
                    zarr_rdr,
                    store,
                    total_rdr,
                    kst_time_list,
                    grp_rdr,
                    grp_kst):
        """
        Description:
            KST 시간대를 zarr에 적재.
        Args:
            zarr_rdr: zarr group, 저장할 zarr 그룹
            store: zarr ZipStore, 파일 저장 인스턴스
            total_rdr: numpy array, 전체 레이더 배열 (time_list_len, RDR_H, RDR_W, 1)
            kst_time_list: list, KST 문자열 시간대 리스트
            grp_rdr: namedtuple, Zarr 레이더 설정 
            grp_kst: namedtuple, Zarr 시간대 설정
        Returns:
            None
        """
        kst_dt = getattr(zarr_rdr, grp_kst.NAME)
        kst_dt[:] = np.array(kst_time_list)
        self.log.info("Save KST time list to zarr zip")
        
        if isinstance(total_rdr, da.Array):
            da.to_zarr(
                arr=total_rdr,
                url=store,
                component=grp_rdr.NAME,
                overwrite=True
            )
        else:
            rdr_ary = getattr(zarr_rdr, grp_rdr.NAME)
            rdr_ary[:] = total_rdr
        self.log.info("Save Radar array to zarr zip")
    
    def save_dat(self, rdr_ary, save_path):
        """
        Description:
            레이더 배열을 dat 파일로 저장.
        Args:
            rdr_ary: numpy array, 전체 레이더 배열 (time_list_len, height, width, 1)
            save_path: str, 파일 저장 경로
        Returns:
            None
        """
        dat_save_path = save_path + ".dat"
        memmap = np.memmap(
            dat_save_path,
            dtype=self.ARRAY_TYPE,
            mode="w+",
            shape=rdr_ary.shape
        )
        memmap[:] = rdr_ary[:]
        self.log.info("Save Radar array to dat file")
    
    def load_dat(self, load_path, metajson):
        """
        Description:
            dat 파일로 저장된 레이더 배열을 불러오기.
        Args:
            load_path: str, 파일 불러올 경로 (확장자 제외)
            metajson: dict, 메타데이터
        Returns:
            rdr_ary: numpy memmap, 전체 레이더 배열 (time_list_len, height, width, 1)
        """
        dat_load_path = load_path + ".dat"
        rdr_ary = np.memmap(
            dat_load_path,
            dtype=getattr(np, metajson["dtype"]),
            shape=tuple(metajson["shape"]),
            mode="r",
            order="C"
        )
        return rdr_ary
    
    def save_metajson(self,
                      rdr_ary_shape,
                      save_path,
                      kst_start_time,
                      kst_end_time,
                      array_dtype,
                      time_step,
                      time_unit):
        """
        Description:
            shape, dtype, 시간 정보 등의 메타데이터 json으로 저장.
        Args:
            rdr_ary_shape: tuple, 레이더 배열 shape
            save_path: str, 파일 저장 경로
            kst_start_time: str, KST 연월일시분(12자리)
            kst_end_time: str, KST 연월일시분(12자리)
            array_dtype: str, 레이더 배열의 데이터 타입
            time_step: int, 시간 간격
            time_unit: str, 시간 간격 단위
        Returns:
            None
        """
        json_save_path = save_path + ".json"
        save_dict = {
            "shape": list(rdr_ary_shape),
            "dtype": array_dtype,
            "kst_start_time": kst_start_time,
            "kst_end_time": kst_end_time,
            "time_step": time_step,
            "time_unit": time_unit,
        }
        save_json(
            save_dict, json_save_path, log=self.log, err_log=self.err_log
        )
    
    def load_metajson(self, load_path):
        """
        Description:
            json 저장된 메타데이터 불러오기.
        Args:
            load_path: str, 파일 불러오기 경로 (확장자 제외)
        Returns:
            metajson: dict, 메타데이터
        """
        json_load_path = load_path + ".json"
        metajson = load_json(
            json_load_path, log=self.log, err_log=self.err_log
        )
        return metajson
    
    def scan_load_files(self):
        """
        Description:
            실제 저장된 파일과 불러올 비교 후 일치하는지 확인.
        Args:
            None
        Returns:
            load_files: list, 불러올 파일 리스트
        """
        reals = [_file for _file in os.listdir(self.saved_dir)]
        reals = sorted(reals)
        if np.all(np.isin(self.usage_extract, reals)):
            self.log.info("Use extracted files:")
            for _file in self.usage_extract:
                self.log.info("    {0}".format(_file))
        else:
            self.log.info("Insufficient files:")
            self.err_log.error("Insufficient files:")
            non_files = list(np.setdiff1d(self.usage_extract, reals))
            for _file in non_files:
                self.log.info("    {0}".format(_file))
                self.err_log.error("    {0}".format(_file))
        load_files = [
            os.path.join(self.saved_dir, _file)
            for _file in self.usage_extract
        ]
        return load_files
    
    def load_zarr_dask(self, load_files, grp_rdr, grp_kst):
        """
        Description:
            저장한 zarr를 dask로 불러오기.
        Args:
            load_files: list, 불러올 파일 리스트
            grp_rdr: namedtuple, Zarr 레이더 설정 
            grp_kst: namedtuple, Zarr 시간대 설정
        Returns:
            rdr_total: dask array, 레이더 dask 배열
            kst_total: numpy array, KST시간대 배열
        """
        rdr_total = [
            da.from_zarr(_zarr, component=grp_rdr.NAME)
            for _zarr in load_files
        ]
        kst_total = [
            da.from_zarr(_zarr, component=grp_kst.NAME)
            for _zarr in load_files
        ]
        rdr_total = da.concatenate(rdr_total, axis=0)
        kst_total = da.concatenate(kst_total, axis=0)
        kst_total = kst_total.compute()
        self.log.info("Total Radar zarr: {0} / {1}".format(rdr_total.shape, rdr_total.dtype))
        return rdr_total, kst_total
    
    def check_kst(self, kst_total, time_unit):
        """
        Description:
            불러온 kst의 시작/종료 시각, time step 확인.
        Args:
            kst_total: numpy array, KST시간대 배열
            time_unit: str, 시간 간격 단위
        Returns:
            kst_start_time: str, KST 연월일시분(12자리)
            kst_end_time: str, KST 연월일시분(12자리)
            time_step: int, 시간 간격
        """
        kst_total_sort = np.sort(kst_total)
        kst_start_time = kst_total_sort[0]
        kst_end_time = kst_total_sort[-1]
        
        if time_unit in ["min", "minute", "minutes", "m"]:
            dtype = "datetime64[m]"
        elif time_unit in ["h", "hr", "hour", "hours"]:
            dtype = "datetime64[h]"
        elif time_unit in ["d", "day", "days"]:
            dtype = "datetime64[D]"
        else:
            self.err_log.error("Check right time unit: {0}".format(time_unit))
        kst_total = np.array(
            [datetime.strptime(t, self.CVT_TIME_FORMAT) for t in kst_total],
            dtype=dtype
        )
        delta_kst = Counter(kst_total[1:] - kst_total[:-1])
        if len(delta_kst) > 1:
            self.log.info("Check zarr files: {0}".format(delta_kst))
        delta_kst_most = delta_kst.most_common(n=1)[0][0]
        time_step = int(delta_kst_most.astype(int))
        
        return kst_start_time, kst_end_time, time_step
    
    def remove_minus(self, rdr_total):
        """
        Description:
            레이더 배열에서 음수를 모두 0으로 치환.
        Args:
            rdr_total: dask array, 레이더 dask 배열
        Returns:
            rdr_nonminus: dask array, 레이더 dask 배열
        """
        self.log.info("Clipping minus to 0")
        return rdr_total.clip(min=0)


class ExtractRawRadar(RadarHSP):
    def __init__(self, cfg, log, err_log):
        """
        Description:
            init은 RadarHSP 에서 상속.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(ExtractRawRadar, self).__init__(cfg, log, err_log)
    
    def __call__(self, kst_start_time, kst_end_time):
        """
        Description:
            레이더 데이터 불러온 뒤 처리 후 저장.
            레이더 스캔 영역만 자르기. (2004x1779 영역 나옴)
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        self.log.info(">>> Start to Extract HSP data")
        self.log.info("Start: {0} / End: {1} (KST)".format(kst_start_time, kst_end_time))
        start_time = time.time()
        
        kst_time_list = self._make_time_list(kst_start_time,
                                             kst_end_time,
                                             self.TIME_UNIT,
                                             self.TIME_STEP)
        save_path = self._get_save_path(
            kst_start_time, kst_end_time, self.TIME_STEP, self.TIME_UNIT,
            self.saved_dir, self.saved_file
        )
        total_rdr = self.make_total_rdr(kst_time_list, self.USE_RAY)
        
        if self.use_zarr:
            self.log.info("Using Zarr")
            zarr_rdr, store = self.save_zarr(
                save_path, total_rdr.shape, self.grp_rdr, self.grp_kst
            )
            self.assign_zarr(
                zarr_rdr, store,
                total_rdr, kst_time_list,
                self.grp_rdr, self.grp_kst
            )
            self.quit_zarr(store)
        else:
            self.log.info("Using Numpy memmap")
            self.save_metajson(
                total_rdr.shape,
                save_path,
                kst_start_time,
                kst_end_time,
                self.ARRAY_TYPE_STR,
                self.TIME_STEP,
                self.TIME_UNIT
            )
            self.save_dat(total_rdr, save_path)
        
        h, m, s = print_elapsed_time(start_time)
        self.log.info("Elapsed time: {0} hour {1} minute {2} second".format(h, m, s))
        self.log.info(">>> End to Extract HSP data")
        return total_rdr
        
    def make_total_rdr(self, kst_time_list, USE_RAY=False):
        """
        Description:
            레이더 데이터를 불러오고 zarr에 적재.
        Args:
            kst_time_list: list, KST 문자열 시간대 리스트
            USE_RAY: bool, ray 사용여부 (default: False)
        Returns:
            total_rdr: numpy array, 전체 레이더 배열 (time_list_len, RDR_H, RDR_W, 1)
        """
        if USE_RAY:
            self.log.info("Use Ray")
            ray_start()
            # @ray.remote에는 self도 argument로 넣어줘야 함
            rdr_list_ids = [
                self.ray_make_rdr.remote(self, kst_time)
                for kst_time in kst_time_list
            ]
            rdr_list = ray.get(rdr_list_ids)
            ray_stop()
        else:
            rdr_list = [
                self.make_rdr(kst_time) for kst_time in kst_time_list
            ]
        total_rdr = np.array(rdr_list, dtype=self.ARRAY_TYPE)
        self.log.info("Data Shape: {0}".format(total_rdr.shape))
        return total_rdr
    
    def make_rdr(self, kst_time):
        """
        Description:
            레이더 데이터로 배열 리스트 생성.
                - 레이더 데이터(bin.gz 파일) 불러오기
                - 레이더 스캔 영역만 자르기
        Args:
            kst_time: str, KST 문자열 시간대
        Returns:
            rdr_ary: numpy array, Slicing한 레이더 강수량 배열 (DOWN-UP, RIGHT-LEFT, 1)
        """
        rdr_ary = self.load_rdr(kst_time)
        if rdr_ary is not None:
            rdr_ary = self.slice_rdr(
                rdr_ary,
                self.EDGE_UP, self.EDGE_DOWN, self.EDGE_LEFT, self.EDGE_RIGHT
            )
            rdr_ary = rdr_ary[..., None]
        else:
            rdr_ary = np.empty((self.RDR_H, self.RDR_W, 1), dtype=self.ARRAY_TYPE)
            # rdr_ary[:] = np.nan
            rdr_ary[:] = -30000
        return rdr_ary
    
    @ray.remote
    def ray_make_rdr(self, kst_time):
        """
        Description:
            레이더 데이터로 배열 리스트 생성. (Ray 이용)
        Args:
            kst_time: str, KST 문자열 시간대
        Returns:
            #rdr_reshape: numpy array, Reshape한 레이더 강수량 배열 (IPT_H, IPT_W, 1)
            rdr_ary: numpy array, Crop한 레이더 강수량 배열 (DOWN - UP, RIGHT - LEFT)
        """
        return self.make_rdr(kst_time)
    
    def load_rdr(self, kst_time, ITER_NUM=1):
        """
        Description:
            레이더 데이터(bin.gz 파일) 불러오기.
            해당 시간대에 데이터가 없으면 5분 전 데이터를 사용.
            MAX_LOOP 만큼 진행해도 없으면 NaN 또는 -30000 배열로 사용.
            데이터 명세에 왼쪽아래->오른위 순으로 되어 위아래 반전 필요.
            원래 강수량에 x100 상태인 int16 상태.
        Args:
            kst_time: str, KST 연월일시분(12자리)
            ITER_NUM: int, 결측 시 반복 횟수 (default: 1)
        Returns:
            rdr_ary: numpy array, 레이더 강수량 배열 (NY, NX)
        """
        kst_yr, kst_m, kst_d = kst_time[:4], kst_time[4:6], kst_time[6:8]
        kst_h, kst_min = kst_time[8:10], kst_time[10:]
        rdr_dir = self.rdr_dir.format(kst_yr, kst_m, kst_d)
        rdr_file = self.rdr_file.format(kst_time)
        try:
            with gzip.open(os.path.join(rdr_dir, rdr_file), "rb") as fg:
                rdr_ary = np.frombuffer(fg.read(), dtype=np.int16)
        except Exception as e:
            self.err_log.error("File cannot open {0} (KST): {1}".format(kst_time, e))
            if ITER_NUM > self.MAX_LOOP:
                rdr_ary = None
                self.log.info("Empty time: {0}.{1}.{2}. {3}:{4}".format(
                    kst_yr, kst_m, kst_d, kst_h, kst_min
                    )
                )
            else:
                kst_time = self._compute_date(kst_time, self.TIME_UNIT, self.FILLNA_TIME_STEP)
                ITER_NUM = ITER_NUM + 1
                rdr_ary = self.load_rdr(kst_time, ITER_NUM)
        else:
            rdr_ary = rdr_ary[self.HEADER_LEN:(self.NX*self.NY + self.HEADER_LEN)]
            rdr_ary = rdr_ary.reshape(self.NY, self.NX, order="C")
            rdr_ary = np.flipud(rdr_ary)   # 위아래 반전
        finally:
            return rdr_ary


class CropRadar(RadarHSP):
    def __init__(self, cfg, log, err_log):
        """
        Description:
            init은 RadarHSP 에서 상속.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(CropRadar, self).__init__(cfg, log, err_log)
        cfg_r = cfg.RAWDATA
        cfg_d = cfg.PARAMS.DATA
        
        # crop mode: Usage Radar Area
        self.USE_H = cfg_d.CROP.H
        self.USE_W = cfg_d.CROP.W
        self.USE_INIT_H = cfg_d.CROP.INIT_H
        self.USE_INIT_W = cfg_d.CROP.INIT_W
        self.saved_crop_dir = os.path.join(self.saved_dir, cfg_r.SAVED_DIR.CROP)
        self.saved_crop_file = cfg_r.SAVED_FILE.CROP
    
    def __call__(self):
        """
        Description:
            레이더 데이터 스캔 영역(zarr) 불러온 뒤 처리 후 저장.
            Random crop 생각해서 input_size보다 큰 영역으로 자르기.
            음수 값은 모두 0으로 변경.
        Args:
            None
        Returns:
            rdr_crop: dask array, 전체 레이더 배열 (time_list_len, CROP_H, CROP_W, 1)
        """
        self.log.info(">>> Start to Crop HSP data")
        start_time = time.time()
        
        # Load zarr
        load_files = self.scan_load_files()
        rdr_total, kst_total = self.load_zarr_dask(load_files, self.grp_rdr, self.grp_kst)
        kst_start_time, kst_end_time, time_step = self.check_kst(kst_total, self.TIME_UNIT)
        self.log.info("Start: {0} / End: {1} (KST)".format(kst_start_time, kst_end_time))
        self.log.info("Time unit: {0} / Time step: {1}".format(self.TIME_UNIT, time_step))
        
        # Crop
        rdr_crop = self.crop_area(rdr_total)
        rdr_crop = self.remove_minus(rdr_crop)
        
        # Save dat
        save_path = self._get_save_path(
            kst_start_time, kst_end_time, time_step, self.TIME_UNIT,
            self.saved_crop_dir, self.saved_crop_file
        )
        self.save_metajson(
            rdr_crop.shape,
            save_path,
            kst_start_time,
            kst_end_time,
            str(rdr_crop.dtype),
            time_step,
            self.TIME_UNIT
        )
        self.save_dat(rdr_crop, save_path)
        
        h, m, s = print_elapsed_time(start_time)
        self.log.info("Elapsed time: {0} hour {1} minute {2} second".format(h, m, s))
        self.log.info(">>> End to Crop HSP data")
        return rdr_crop
    
    def crop_area(self, rdr_total):
        """
        Description:
            레이더 배열을 잘라내기.
        Args:
            rdr_total: dask array, 레이더 dask 배열
        Returns:
            rdr_crop: dask array, 레이더 dask 배열
        """
        start_h, end_h = self.USE_INIT_H, self.USE_INIT_H + self.USE_H
        start_w, end_w = self.USE_INIT_W, self.USE_INIT_W + self.USE_W
        rdr_crop = rdr_total[:, start_h:end_h, start_w:end_w, :]
        return rdr_crop


class MaxpoolRadar(RadarHSP):
    def __init__(self, cfg, log, err_log):
        """
        Description:
            init은 RadarHSP 에서 상속.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(MaxpoolRadar, self).__init__(cfg, log, err_log)
        cfg_r = cfg.RAWDATA
        
        # maxpool mode: Usage Radar Area
        self.saved_maxp_dir = os.path.join(self.saved_dir, cfg_r.SAVED_DIR.MAXPOOL)
        self.saved_maxp_file = cfg_r.SAVED_FILE.MAXPOOL
    
    def __call__(self):
        """
        Description:
            레이더 데이터 스캔 영역(zarr) 불러온 뒤 처리 후 저장.
            input_size(256x256)의 정수배로 만들기 위해 padding 후 진행.
            Maxpooling으로 window의 최대값만 추출.
                - raw 값은 유지한 채로 해상도 변경
            음수 값은 모두 0으로 변경.
        Args:
            None
        Returns:
            rdr_maxp: dask array, 전체 레이더 배열 (time_list_len, IPT_H, IPT_W, 1)
        """
        self.log.info(">>> Start to Maxpool HSP data")
        start_time = time.time()
        
        # Load zarr
        load_files = self.scan_load_files()
        rdr_total, kst_total = self.load_zarr_dask(load_files, self.grp_rdr, self.grp_kst)
        kst_start_time, kst_end_time, time_step = self.check_kst(kst_total, self.TIME_UNIT)
        self.log.info("Start: {0} / End: {1} (KST)".format(kst_start_time, kst_end_time))
        self.log.info("Time unit: {0} / Time step: {1}".format(self.TIME_UNIT, time_step))
        
        # Maxpooling
        WINDOW = self.compute_window(self.IPT_H, self.IPT_W, rdr_total.shape)
        rdr_pad = self.padding_area(rdr_total, self.IPT_H, self.IPT_W, WINDOW)
        rdr_maxp = self.maxpool_area(rdr_pad, self.IPT_H, self.IPT_W, WINDOW)
        rdr_maxp = self.remove_minus(rdr_maxp)
        
        # Save dat
        save_path = self._get_save_path(
            kst_start_time, kst_end_time, time_step, self.TIME_UNIT,
            self.saved_maxp_dir, self.saved_maxp_file
        )
        self.save_metajson(
            rdr_maxp.shape,
            save_path,
            kst_start_time,
            kst_end_time,
            str(rdr_maxp.dtype),
            time_step,
            self.TIME_UNIT
        )
        self.save_dat(rdr_maxp, save_path)
        
        h, m, s = print_elapsed_time(start_time)
        self.log.info("Elapsed time: {0} hour {1} minute {2} second".format(h, m, s))
        self.log.info(">>> End to Maxpool HSP data")
        return rdr_maxp
    
    def compute_window(self, IPT_H, IPT_W, rdr_shape):
        """
        Description:
            Pooling 적용할 window 계산.
        Args:
            IPT_H, IPT_W: int, 모델 입력 Height, Width
            rdr_shape: tuple, 레이더 dask 배열의 shape
        Returns:
            WINDOW: tuple, window size (h, w)
        """
        WINDOW_H = rdr_shape[1] // IPT_H + 1
        WINDOW_W = rdr_shape[2] // IPT_W + 1
        self.log.info("Maxpool window: ({0}, {1})".format(WINDOW_H, WINDOW_W))
        return (WINDOW_H, WINDOW_W)
    
    def padding_area(self, rdr_total, IPT_H, IPT_W, WINDOW):
        """
        Description:
            window의 정수 배가 되도록 레이더 배열을 zero padding.
            레이더 영역의 위쪽, 왼쪽만 padding 진행.
        Args:
            rdr_total: dask array, 레이더 dask 배열 
            IPT_H, IPT_W: int, 모델 입력 Height, Width
            WINDOW: tuple, window size (h, w)
        Returns:
            rdr_pad: dask array, zero-padding한 레이더 dask 배열
        """
        _, _h, _w, __ = rdr_total.shape
        WINDOW_H, WINDOW_W = WINDOW
        BF_MAXP_H, BF_MAXP_W = IPT_H * WINDOW_H, IPT_W * WINDOW_W
        # Zero padding
        CHUNK_NUM = rdr_total.chunks[0][0]
        pad_udlr = (
            (0, 0),
            (BF_MAXP_H - _h, 0),   # height
            (BF_MAXP_W - _w, 0),   # width
            (0, 0)
        )
        self.log.info("Model Input size: Height {0} / Width {1})".format(IPT_H, IPT_W))
        self.log.info("Zero-Padding UP: {0}".format(BF_MAXP_H - _h))
        self.log.info("Zero-Padding LEFT: {0}".format(BF_MAXP_W - _w))
        rdr_pad = da.pad(
            rdr_total,
            pad_width=pad_udlr,
            mode="constant",
            constant_values=0
        )
        rdr_pad = da.rechunk(
            rdr_pad,
            chunks=(
                min(CHUNK_NUM, rdr_pad.shape[0]),
                BF_MAXP_H, BF_MAXP_W, 1
            )
        )
        self.log.info("Zero-Padding Radar: {0}".format(rdr_pad.shape))
        return rdr_pad
    
    def maxpool_area(self, rdr_pad, IPT_H, IPT_W, WINDOW):
        """
        Description:
            zero padding된 레이더 영역에 대해 Maxpooling 진행.
        Args:
            rdr_pad: dask array, zero-padding한 레이더 dask 배열
            IPT_H, IPT_W: int, 모델 입력 Height, Width
            WINDOW: tuple, window size (h, w)
        Returns:
            rdr_maxp: dask array, maxpooling한 레이더 dask 배열
        """
        nums, _, __, chn = rdr_pad.shape
        WINDOW_H, WINDOW_W = WINDOW
        rdr_reshape = da.reshape(
            rdr_pad,
            (nums, IPT_H, WINDOW_H, IPT_W, WINDOW_W, chn)
        )
        rdr_maxp = da.max(rdr_reshape, axis=(2, 4))
        self.log.info("Maxpooling Radar: {0}".format(rdr_maxp.shape))
        return rdr_maxp


class EmbedRadar(MaxpoolRadar):
    def __init__(self, cfg, log, err_log):
        """
        Description:
            init은 MaxpoolRadar 에서 상속.
            window 및 padding 때문에 MaxpoolRadar 이용.
                - Maxpool 없어진다면 해당 함수 옮겨와야 함
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(EmbedRadar, self).__init__(cfg, log, err_log)
        cfg_r = cfg.RAWDATA
        
        # embed mode: Usage Radar Area
        self.grp_rdr_embd = cfg_r.ZARR_GROUP.EMBED
        self.saved_embd_dir = os.path.join(self.saved_dir, cfg_r.SAVED_DIR.EMBED)
        self.saved_embd_file = cfg_r.SAVED_FILE.EMBED
    
    def __call__(self):
        """
        Description:
            레이더 데이터 스캔 영역(zarr) 불러온 뒤 처리 후 저장.
                - dat으로 하기엔 용량 문제로 zarr 저장
            input_size(256x256)의 정수배로 만들기 위해 padding 후 진행.
            window를 channel로 보냄.
                - raw 값은 유지한 채로 window_h x window_w 만큼의 채널
            음수 값은 모두 0으로 변경.
        Args:
            None
        Returns:
            rdr_embd: numpy array, 전체 레이더 배열 (time_list_len, IPT_H, IPT_W, Channel)
        """
        self.log.info(">>> Start to Embed HSP data")
        start_time = time.time()
        
        # Load zarr
        load_files = self.scan_load_files()
        rdr_total, kst_total = self.load_zarr_dask(load_files, self.grp_rdr, self.grp_kst)
        kst_start_time, kst_end_time, time_step = self.check_kst(kst_total, self.TIME_UNIT)
        self.log.info("Start: {0} / End: {1} (KST)".format(kst_start_time, kst_end_time))
        self.log.info("Time unit: {0} / Time step: {1}".format(self.TIME_UNIT, time_step))
        
        # Embedding
        WINDOW = self.compute_window(self.IPT_H, self.IPT_W, rdr_total.shape)
        rdr_pad = self.padding_area(rdr_total, self.IPT_H, self.IPT_W, WINDOW)
        rdr_embd = self.embed_area(rdr_pad, self.IPT_H, self.IPT_W, WINDOW)
        rdr_embd = self.remove_minus(rdr_embd)
        
        # Save Zarr
        self.log.info("Using Zarr")
        save_path = self._get_save_path(
            kst_start_time, kst_end_time, time_step, self.TIME_UNIT,
            self.saved_embd_dir, self.saved_embd_file
        )
        zarr_rdr, store = self.save_zarr(
            save_path, rdr_embd.shape, self.grp_rdr_embd, self.grp_kst
        )
        self.assign_zarr(
            zarr_rdr, store,
            rdr_embd, kst_total,
            self.grp_rdr_embd, self.grp_kst
        )
        self.quit_zarr(store)
        
        h, m, s = print_elapsed_time(start_time)
        self.log.info("Elapsed time: {0} hour {1} minute {2} second".format(h, m, s))
        self.log.info(">>> End to Embed HSP data")
        return rdr_embd
    
    def embed_area(self, rdr_pad, IPT_H, IPT_W, WINDOW):
        """
        Description:
            zero padding된 레이더 영역에 대해 Embedding 진행.
            Input size 대로 만들며 raw 값을 모두 channel로 변경
                - Before: (nums, IPT_H x WINDOW_H, IPT_W x WINDOW_W, 1)
                - After: (nums, IPT_H, IPT_W, WINDOW_H x WINDOW_W)
                - 바로 최종 형상으로 reshape이 불가능하여 중간 단계 거쳐야 함
        Args:
            rdr_pad: dask array, zero-padding한 레이더 dask 배열
            IPT_H, IPT_W: int, 모델 입력 Height, Width
            WINDOW: tuple, window size (h, w)
        Returns:
            rdr_embd: dask array, embedding한 레이더 dask 배열
        """
        nums, _, __, chn = rdr_pad.shape
        WINDOW_H, WINDOW_W = WINDOW
        rdr_embd = da.reshape(
            rdr_pad,
            (nums, IPT_H, WINDOW_H, IPT_W, WINDOW_W, chn)
        )
        rdr_embd = da.reshape(
            da.moveaxis(rdr_embd, 2, 3),   # WINDOW_H <=> IPT_W
            (nums, IPT_H, IPT_W, WINDOW_H * WINDOW_W * chn)
        )
        self.log.info("Embedding Radar: {0}".format(rdr_embd.shape))
        return rdr_embd
