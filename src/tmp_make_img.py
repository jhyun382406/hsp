"""
기상레이더센터 색범례 적용한 이미지 생성.

*** 사용 예시
RAIN_CONST = rain_const()
rain_mm_range, rain_rgba = load_rain_color(*RAIN_CONST)
# rdr_ary = np.transpose(rdr_ary, (1, 2, 0))   # channel_first면 필요
rdr_rgba = make_rgba_img(rdr_ary, rain_mm_range, rain_rgba)
plot_radarframe(rdr_rgba)
"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


def rain_const():
    COLOR_CFG_PATH = "./wizai_configs/rain_color.csv"
    COLOR_CFG_COLS = ["rain_gte", "rain_lt", "r", "g", "b", "a"]
    COLOR_CFG_TYPE = ["float32", "float32", "uint8", "uint8", "uint8", "uint8"]
    return (COLOR_CFG_PATH, COLOR_CFG_COLS, COLOR_CFG_TYPE)


def load_rain_color(color_cfg_path, color_cfg_cols, color_cfg_type):
    """
    Description:
        강수 이미지에 넣을 색범례 파일 불러오기.
    Args:
        None
    Returns:
        rain_mm_range: numpy array, 강수량 mm, 이상/미만 기준
        rain_bgra: numpy array, 강수량 범례별 BGRA
    """
    dtype = {
        k: getattr(np, v)
        for k, v 
        in zip(color_cfg_cols, color_cfg_type)
    }
    rain_color_df = pd.read_csv(color_cfg_path,
                                encoding="utf-8",
                                dtype=dtype)
    rain_mm_range = rain_color_df[color_cfg_cols[:2]].to_numpy()
    # # RGBA를 BGRA로 변환(OpenCV 기본이 BGRA)
    # bgra = color_cfg_cols[2:]
    # bgra[0], bgra[2] = bgra[2], bgra[0]
    # rain_bgra = rain_color_df[bgra].to_numpy()
    rgba = color_cfg_cols[2:]
    rain_rgba = rain_color_df[rgba].to_numpy()
    return rain_mm_range, rain_rgba


def make_rgba_img(ary, rain_mm_range, rain_rgba):
    """
    Description:
        개별 BGRA 강수 이미지 배열 생성.
    Args:
        ary: numpy array, 강수량 배열
        rain_mm_range: numpy array, 강수량 mm, 이상/미만 기준
        rain_bgra: numpy array, 강수량 범례별 BGRA
    Returns:
        rain_img: numpy array, 강수량 BGRA 배열
    """
    image_shape = list(ary.shape)
    image_shape[-1] = 4   # rainfall -> BGRA
    rain_img = np.zeros(image_shape, dtype=np.uint8)
    
    # 최대 강수량으로 for문 제한
    max_idx = np.argmax(rain_mm_range[:, 1] > np.max(ary))
    # BGRA로 빈 이미지 채우기
    for idx in range(max_idx)[1:]:
        gte_cond = (ary >= rain_mm_range[idx, 0])
        lt_cond = (ary < rain_mm_range[idx, 1])
        rain_img[np.where(gte_cond & lt_cond)[:-1]] = rain_rgba[idx]   # 채널로 인해 -1 인덱싱
    
    return rain_img


def plot_radarframe(rdrfr, figsize=(5, 5)):
    """
    Description:
        개별 강수 이미지 도식.
    Args:
        rdrfr: numpy array, 강수 이미지 배열
        figsize: tuple, (width, height)
    Returns:
        None
    """
    plt.rc("figure", figsize=figsize)
    plt.rc("figure", dpi=100)
    plt.rc("font", size=14)  # controls default text sizes
    plt.rc("axes", titlesize=14)  # fontsize of the axes title
    plt.rc("axes", labelsize=14)  # fontsize of the x and y labels
    plt.rc("xtick", labelsize=6)  # fontsize of the tick labels
    plt.rc("ytick", labelsize=6)  # fontsize of the tick labels
    
    plt.title("HSP Rainfall")
    plt.imshow(rdrfr)