"""
HSP Dataloader.

레이더 배열 데이터(int16, 원래 수의 x100)를 원래 float로 변경.
Frame에서 강수 영역이 5% 이상인 경우만 데이터 사용
pretrained를 쓰려면 openclimatefix 기본 설정대로 진행.
사용하지 않는다면 input, target length 변경도 가능

클래스 설명
    ########- Data: 파일 시간대 관련
    ########- RadarHSP: HSP 데이터 처리 상위 클래스
    ########- ExtractRawRadar: bin.gz -> zarr 또는 dat 저장. 레이더 영역만 슬라이싱.
    ########- CropRadar: zarr-> dat 저장. 일부 영역만 crop.
    ########- MaxpoolRadar: zarr -> dat 저장. padding 후 maxpooling 적용.
    ########- EmbedRadar: zarr -> dat 저장. padding 후 patch embedding 아이디어 적용하여 원본 데이터 보존.
"""