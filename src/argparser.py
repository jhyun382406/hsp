"""
main.py 의 argparse 설정.
"""


def make_args(parser):
    """
    Description:
        파이썬 Argparse 설정.
    Args:
        parser: ArgumentParser, 파서
    Returns:
        args: Namespace, 파싱된 인자 모음
    """
    parser.add_argument(
        "-c", "--config", type=str,
        help="a config file in yaml format to control experiment"
    )
    parser.add_argument(
        "-m", "--mode", type=str,
        # choices=["train", "test", "predict", "valid", "image", "data"]
        choices=[
            "data-raw", "data-crop", "data-maxp", "data-embed",
            "prep", "train", "infer"
        ]
    )
    parser.add_argument(
        "-st", "--start_time", type=str, default=None,
        help="start time used for prediction (KST) e.g. 202203150700"
    )
    parser.add_argument(
        "-et", "--end_time", type=str, default=None,
        help="end time used for prediction (KST) e.g. 202203150850"
    )
    args = parser.parse_args()
    return args