"""
Ray 기본 세팅.
"""
import json
import ray


SPILL_RAY_PATH = "/ray-temp"

def ray_start():
    """
    Description:
        Ray 시작.
    """
    if not ray.is_initialized():
        ray.init(
            _system_config={
                "max_io_workers": 4,  # More IO workers for parallelism.
                # Allow spilling until the local disk is 98% utilized.
                # This only affects spilling to the local file system.
                "local_fs_capacity_threshold": 0.98,   # default: 0.95
                "object_spilling_config": json.dumps(
                    {
                        "type": "filesystem",
                        "params": {
                            "directory_path": SPILL_RAY_PATH,
                            # HDD has a large buffer size (> 1MB)
                            "buffer_size": 100 * 1024 * 1024, # Use a 100MB buffer for writes
                        }
                    },
                )
            },
        )


def ray_stop():
    """
    Description:
        Ray 종료.
    """
    if ray.is_initialized():
        ray.shutdown()
