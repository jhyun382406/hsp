import os
import yaml
import json
import logging
from collections import namedtuple
import numpy as np


def convert(
    file_path: str=None,
    dictionary: dict=None
    ):
    """
    Description:
        코드에 적용할 설정 YAML 파일 불러오기.
    Args:
        file_path, str, yaml 파일 경로
        dictionary, dict, 반환시킬 딕셔너리
    Returns:
        GenericDict: namedtuple, yaml 파일을 GenericDict로 바꾼 것(수정불가)
    """
    if file_path is not None:
        assert file_path.endswith("yaml"), "the file should be .yaml format"
        with open(file_path, "r") as f:
            dictionary = yaml.full_load(f)
    if dictionary is not None:
        for k in dictionary.keys():
            if isinstance(dictionary[k], dict):
                dictionary[k] = convert(dictionary=dictionary[k])
        return namedtuple("GenericDict", dictionary.keys())(**dictionary)


def get_arguments(
    params: namedtuple=None
    ):
    args = {}
    for k in params._asdict().keys():
        args[k.lower()] = params._asdict()[k]
    return args


def logprint(value, log=None, level="info"):
    """
    Description:
        로그 출력 또는 파이썬 프린트.
    Args:
        value: 출력할 값
        log: logging.Logger, 로그 인스턴스 (default: None)
        level: str, 로그 레벨 (default: info)
    Returns:
        None
    """
    if isinstance(log, logging.Logger):
        getattr(log, level)(value)
    else:
        print(value)


def return_dir(is_making=False, *args):
    """
    Description:
        디렉토리 경로 반환.
        해당 경로에 디렉토리가 없으면 생성.
    Args:
        *args: str, 디렉토리 생성할 개별 명칭
    Returns:
        dir_path: str, 디렉토리 경로
    """
    dir_path = os.path.join(*args)
    if not os.path.isdir(dir_path) and is_making:
        os.makedirs(dir_path)
    return dir_path


def save_json(save_dict,
              save_path,
              encoding="utf-8",
              log=None,
              err_log=None):
    """
    Description:
        json 파일 저장.
    Args:
        save_dict: dict, 저장할 딕셔너리
        save_path: str, 저장할 파일 경로
        encoding: str, json 파일 인코딩 방식 (default="utf-8")
        log: logging.logger, 로그 객체 (default=None)
        err_log: logging.logger, 에러 로그 객체 (default=None)
    Returns:
        None
    """
    try:
        with open(save_path, "w", encoding=encoding) as p:
            json.dump(save_dict, p, ensure_ascii=False, indent="\t")
    except Exception as e:
        logprint("Save json file incompletely", log)
        logprint("Save json file incompletely: {0}".format(e), err_log, "error")
        raise e
    else:
        logprint("Save json file completely", log)


def load_json(load_path,
              encoding="utf-8",
              log=None,
              err_log=None):
    """
    Description:
        json 파일 불러오기.
    Args:
        load_path: str, 불러올 파일 경로
        encoding: str, json 파일 인코딩 방식 (default="utf-8")
        log: logging.logger, 로그 객체 (default=None)
        err_log: logging.logger, 에러 로그 객체 (default=None)
    Returns:
        load_dict: dict, 불러온 딕셔너리
    """
    try:
        with open(load_path, "r", encoding=encoding) as f:
            load_dict = json.load(f)
    except Exception as e:
        logprint("Load json file incompletely", log)
        logprint("Load json file incompletely: {0}".format(e), err_log, "error")
        raise e
    else:
        logprint("Load json file completely", log)
        return load_dict


def combined_indices_of_cropped(origin_data_shape, model_input_shape):
    """
    Description:
        잘라낸 이미지로 전체 이미지 복원 위해 시작 index 계산.
    Args:
        origin_data_shape: tuple, 원본 영역 데이터 shape
        model_input_shape: tuple, 모델 입력 shape
    Returns:
        h_start_indices: list, 병합 이미지의 Height 쪽 시작 index
        w_start_indices: list, 병합 이미지의 Width 쪽 시작 index
    """
    def _make_start_indices(d_len, ipt_len):
        """
        Description:
            시작 index 계산.
        Args:
            d_len: int, 원본 영역 데이터의 길이
            ipt_len: int, 모델 입력 데이터의 길이
        Returns:
            start_indices: list, 시작 index 리스트
        """
        # 가로세로 몇 개 나눌지
        num = d_len // ipt_len + 1
        # 겹치는 부분 처리
        total_res = num * ipt_len - d_len
        res = total_res // (num - 1) + 1
        # 겹치는 부분 리스트 생성, 마지막 부분은 원본 영역에 맞게 조정
        res_list = [res if not i == 0 else i for i in range(num)]
        if sum(res_list) > total_res:
            res_list[-1] -= sum(res_list) - total_res
        start_indices = [
            i * ipt_len - res
            for i, res in enumerate(np.cumsum(res_list))
        ]
        return start_indices
        
    _, data_h, data_w = origin_data_shape
    input_h, input_w, _ = model_input_shape

    # 세로 방향
    if data_h == input_h:
        h_start_indices = [0]
    else:
        h_start_indices = _make_start_indices(data_h, input_h)
    # 가로 방향
    if data_w == input_w:
        w_start_indices = [0]
    else:
        w_start_indices = _make_start_indices(data_w, input_w)
    
    return h_start_indices, w_start_indices
