import random
import numpy as np
import torch


def set_gpu_torch(gpu_idx, n_GB):
    """
    Description:
        Pytorch에서 GPU 및 메모리 할당.
    Args:
        gpu_idx: int, GPU index 값
        n_GB: float, 할당할 메모리 크기(GB)
    Returns:
        device: torch.device, 할당된 GPU
    """
    able_to_use_gpu = torch.cuda.is_available()
    device = torch.device("cuda:{0}".format(gpu_idx)
                          if able_to_use_gpu else "cpu")
    TOTAL_GPU_MEM = torch.cuda.get_device_properties(gpu_idx).total_memory
    ALOC_GPU_MEM_BYTE = np.min([n_GB * 1024 * 1024 * 1024, TOTAL_GPU_MEM])
    ALOC_GPU_RATIO = ALOC_GPU_MEM_BYTE / TOTAL_GPU_MEM
    if able_to_use_gpu:
        torch.cuda.set_device(device)
        torch.cuda.set_per_process_memory_fraction(ALOC_GPU_RATIO, gpu_idx)
    return device


def set_random_seed(random_seed, if_cudnn=False, if_multi=False):
    """
    Description:
        Pytorch에서 RANDOM SEED 고정.
    Args:
        random_seed: int, 설정할 random seed 값
        if_cudnn: bool, CuDNN 쪽 randomness 제어 여부, 속도가 느려져 최후반부에 반영 필요 (default=False)
        if_multi: bool, Multi-GPU 사용 시 (default=False)
    Returns:
        None
    """
    torch.manual_seed(random_seed)   # Pytorch
    torch.cuda.manual_seed(random_seed)   # Pytorch
    np.random.seed(random_seed)   # Numpy
    random.seed(random_seed)   # Python
    if if_cudnn:
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False
    if if_multi:
        torch.manual_seed_all(random_seed)


def set_float32_matmul(precision="highest"):
    """
    Description:
        Pytorch에서 MatMul 때 Float32 설정.
        https://pytorch.org/docs/stable/generated/torch.set_float32_matmul_precision.html#torch.set_float32_matmul_precision
    Args:
        precision: str, highest, high, medium 만 가능 (default="highest")
            - high, medium 순으로 연산 속도 빨라짐
            - 불가능 연산은 상위 precision 이용
            - bfloat16 데이터 타입을 이용하는 것으로 보임
    Returns:
        None
    """
    torch.set_float32_matmul_precision(precision)