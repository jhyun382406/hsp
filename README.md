# Predict Rainfall Images Sequence using HSP data

[[_TOC_]]

## Name
- DGMR(Deep Generative Model of Radars) from DeepMind in PyTorch
- [Openclimatefix의 Github 코드](https://github.com/openclimatefix/skillful_nowcasting) 를 받아 테스트 중
- [DGMR 원래 논문](https://arxiv.org/abs/2104.00954)

## Description
- 기본적으로 강수량 시계열 데이터(2차원 이미지 형식)를 이용하여 강수량을 생성
- 원본 HSP(강수량) 사용하도록 Dataloader 변경
- 원본(2881x2305)에서 레이더 영역(2004x1779)도 모델 입력으로 크기 때문에 size 조정
  - CROP 방식 (256x256x1, 임의로 경기-충청 지역 설정)
  - Maxpooling 방식 (256x256x1, 위쪽, 왼쪽으로 zero-padding 후 진행)
  - Embedding 방식 (256x256x56, 위쪽, 왼쪽으로 zero-padding 후 진행)
- 사용 데이터
  - 시간 간격: 10분 간격
  - input size: 256 x 256
  - 입/출력 시계열 데이터 길이 (pre-trained): 4 / 18

## Environment
- Base 도커 이미지: pytorch/pytorch:2.0.1-cuda11.7-cudnn8-runtime
- 우분투 버전: Ubuntu 20.04.6 LTS

|사용 라이브러리|버전|
|:---:|---:|
|Python|3.10.11|
|numpy|1.26.3|
|scikit-learn|1.3.2|
|torch|2.0.1|
|pytorch-lightning|2.1.3|
|dask|2024.1.1|
|zarr|2.16.1|
|ray|2.8.1|

- 이외 파이썬 라이브러리: requirements.txt

## Execution
레이더 배열 데이터 추출  
```bash
python main.py -c /path/of/config/file -m data-raw -st yyyymmddhhmi -et yyyymmddhhmi
```  
추출한 배열 데이터로 input size에 맞추어 크기 조정  
```python
# Crop
python main.py -c /path/of/config/file -m data-crop
# Maxpooling
python main.py -c /path/of/config/file -m data-maxp
# Embedding
python main.py -c /path/of/config/file -m data-embed
```  

## File Structure
- dgmr/, tests/, train/: openclimaxfix 쪽의 원본 dgmr 코드
- pretrained/: openclimaxfix 쪽에서 학습한 pre-trained 모델 적재
- dockerfile/: 도커 환경설정한 yaml 파일들
- log/, err_log/: 로그 및 에러 로그 파일 적재
- hsp_datas/: 레이더 데이터(HSP) 처리 파일 적재
- configs/: 설정 파일 경로
  - hsp_cfg.yaml: 설정 yaml 파일
  - rain_color.csv: 강수량 이미지 만들 때 사용하는 색상 (기상레이더센터 범례 색상 기준)
- src/: 소스코드 경로
  - argparser.py: main 파일의 argument parser 설정
  - extract_data.py: 데이터 추출
  - make_dl.py: 전처리 및 dataloader 생성
  - tmp_make_img.py: 레이더 배열을 색범례에 맞추어 그림으로 변경
- utils/: 유틸코드 경로
  - code_utils.py: 자주 사용하는 유틸 함수
  - log_module.py: 로그 모듈
  - ray_setting.py: ray 라이브러리 세팅
  - torch_settings.py: torch 라이브러리 세팅
- main.py: 실행하는 main 파일
- requirements.txt: 사용한 환경에서의 파이썬 라이브러리

## Scheduled Tasks
- 강수량 5% 미만인 frame들은 제외 후 Sequence 만들기
  - Sequence 내 모든 frame이 강수량 5% 이상인 경우
  - Sequence 내 1개의 frame이라도 강수량 5% 이상인 경우
- Dataloader 생성
  - float로 변환
  - input, target 길이는 우선 pre-trained와 동일하게 진행
- Pre-trained로부터 fine-tuning 하는 식으로 우선 진행

## Additional Research
- GPU 분산 학습
- Pre-trained 적용 / 미적용 비교
- 현재 HSR, HSP 데이터의 픽셀 별 고도, Land/Sea Mask 데이터는 미확보 상태
  - 픽셀 별 위/경도 데이터만 있으면 수집 가능
  - 데이터 확보되면 input으로 추가해볼 수 있음
